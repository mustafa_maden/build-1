#!/bin/bash
set -e

# variables.
notificationtype=${1}
# custom parameters.
param1=${2}
param2=${3}

# set slack channel.
[[ "${SLACK_CHANNEL}" != "" ]] && slackChannel="${SLACK_CHANNEL}" || slackChannel="hodor"

# check slack url is not empty.
[[ "${SLACK_URL}" == "" ]] && { echo "Slack url could not found on build environment variables!"; exit 1; };

# code coverage failed notify.
if [[ "${notificationtype}" == "code-coverage-failed" ]]; then
    curl -X POST \
    $SLACK_URL \
    -d "{
        'channel': '#${slackChannel}',
        'text': 'Opps!',
        'username': 'AppVeyor',
        'attachments': [
            {
                'author_name': '${APPVEYOR_PROJECT_NAME}',
                'author_link': 'https://ci.appveyor.com/project/CiceksepetiIntHizAS/${APPVEYOR_PROJECT_NAME}',
                'color': 'danger',
                'title': 'Build Failed!'
            },
            {
                'color': 'warning',
                'text': 'Code coverage is lower then expected.'
            },
            {
                'fields': [
                    {
                        'title': 'Current',
                        'value': '%${param1}',
                        'short': true
                    },
                    {
                        'title': 'Expected',
                        'value': '%${param2} (min.)',
                        'short': true
                    }
                ],
                'footer': 'Code Coverage Report from OpenCover.'
            }
        ]
    }"
fi